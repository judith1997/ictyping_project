from datetime import datetime
from pytz import timezone
from pathlib import Path
from Bio import SeqIO
from shutil import rmtree
from Bio.Blast import NCBIWWW
from Bio.Blast import NCBIXML
import urllib
import time


class NCBIControler:
    """
    NCBI API says the following in it's guidelines:
    - Do not contact the server more often than once every 10 seconds.
    - Do not poll for any single RID more often than once a minute.
    - Run scripts weekends or between 9 pm and 5 am Eastern time on weekdays
      if more than 50 searches will be submitted.
    - BLAST often runs more efficiently if multiple queries are sent as one
      search rather than if each query is sent as an individual search.
      This is especially true for blastn, megablast, and tblastn. If your
      queries are short (less than a few hundred bases) we suggest you merge
      them into one search of up to 1,000 bases
    """
    def __init__(self, ip, op):
        self.ip = ip
        self.op = op
        # create temporary directory path
        self.tmp = ip.parent.joinpath("tmp")
        self.tmp.mkdir(exist_ok=True)

    def valid_time(self):
        """
        Check if the current time confirms with NCBI API's Guidelines for large
        batch BLASTs.
        """
        # get time object
        est = timezone("US/Eastern")  # timezone required by NCBI
        est_time = datetime.now(est)
        # check if it is weekend
        # weekday returns day numbered from 0 to 6
        if est_time.today().weekday() > 4:
            is_weekend = True
        else:
            is_weekend = False
        # check if we are between 2100 and 0500
        # hours in ISO standard (0-23)
        if est_time.hour < 5 or est_time.hour >= 21:
            valid_time = True
        else:
            valid_time = False
        # return true if we are in a valid timespan
        if is_weekend or valid_time:
            return True
        else:
            return False

    def get_next_valid_time(self):
        """
        Return the amount of seconds to wait for the next valid ncbi timeslot
        """
        # if current time is valid timeslot no waiting is required
        # get current time EST
        if self.valid_time():
            return 0
        else:
            est = timezone("US/Eastern")  # timezone required by NCBI
            est_time = datetime.now(est)
            start_slot = datetime.now(est)
            #  select the next startslot
            start_slot = start_slot.replace(hour=21, minute=0, second=0)
            # the remaining time in seconds
            remaining_time = start_slot - est_time
            # convert to seconds for sleep function
            return int(remaining_time.total_seconds())

    def generate_fragment_batches(self, filepath):
        # create tmp directory
        batch_folder = filepath.stem.split(".")[0].split("data")[-1]
        # initialize variables for batching
        records = []
        record_bases = 0
        record_id = 0
        # loop over all records in file and split into batches if needed
        self.tmp.joinpath(batch_folder).mkdir(exist_ok=True)
        for record in SeqIO.parse(filepath, "fasta"):
            record_bases += len(record.seq)
            # if this sequence makes us reach 1000 basepairs
            if record_bases > 1000:
                record_bases = len(record.seq)
                # store current batch
                SeqIO.write(records,
                            self.tmp.joinpath(batch_folder
                                              ).joinpath("{}_{}.FASTA".format(
                                                            record_id,
                                                            filepath.stem)),
                            "fasta")
                # clear batch up the id number with one
                records = []
                record_id += 1
            else:
                records.append(record)
        # write last record if it isnt empty
        if records != []:
            SeqIO.write(records,
                        self.tmp.joinpath(batch_folder
                                          ).joinpath("{}_{}.FASTA".format(
                                                        record_id,
                                                        filepath.stem)),
                        "fasta")
        return self.tmp.joinpath(batch_folder)

    def run_blast(self, filepath, run_webblast=False):
        # run blast for all batch files
        result_file = "identifiers"
        # path and filename identifiers
        frag_len_id = filepath.stem.split("_")[-2]
        frag_num=filepath.stem.split('_')[-1]
        result_path = filepath.joinpath(str(frag_len_id)+"_"+result_file+'_'+str(frag_num))

        for file in list(filepath.glob("*.FASTA")):
            with open(file, "r") as fasta_file:
                sequences = fasta_file.read()
                fasta_file.close()
            # Blast takes +- 30 to 300 seconds
            print("starting blast")
            handle_path = filepath.joinpath(file.stem.split(".")[0] +
                                            ".handle.xml")
            f = True
            while f:
                try:
                    print("Valid timeslot:", self.valid_time())
                    if self.valid_time():
                        result_handle = NCBIWWW.qblast("blastn",
                                                       "nt",
                                                       sequences,
                                                       hitlist_size=1)
                        with open(handle_path, "w") as f:
                            f.write(result_handle.read())
                        f = False
                        # Wait 10 seconds before sending a new request
                        time.sleep(10)
                    # If we are not in a valid timeslot
                    else:
                        # Try again at the next valid timeslot
                        print("Waiting for next timeslot")
                        print(self.get_next_valid_time())
                        time.sleep(self.get_next_valid_time())
                        print("Time slot reached, proceeding")
                except urllib.error.URLError:
                    print("urlerror, trying again")
                    # Wait 10 seconds before sending a new request
                    time.sleep(10)
            print("blast completed")
            result_handle = open(handle_path)
            blast_results = NCBIXML.parse(result_handle)
            # for every sequence we blasted
            group_names = [line for line in sequences.split("\n")
                           if ">" in line]
            i = 0
            print("path exists:", result_path.exists())
            if not result_path.exists():
                with open(result_path, "w") as f:
                    f.write("group\tid\tseq_start\tseq_end\n")
            with open(result_path, "a") as f:
                for blast_result in blast_results:
                    # for every hit for each blasted sequence
                    for hit in blast_result.alignments:
                        # get the first hps in the hit
                        hsp = hit.hsps[0]
                        # create accesion of specie
                        identifier = hit.title.split("|")[3]
                        # create request link
                        # for the request we have to do the region highest
                        # to lowest
                        region = sorted([hsp.sbjct_start, hsp.sbjct_end])
                        # write request link to file
                        f.write("{}\t{}\t{}\t{}\n".format(group_names[i],
                                                          identifier,
                                                          region[0],
                                                          region[1]))
                    i += 1
            result_handle.close()
            handle_path.unlink()  # delete temporary handle file
        # when fragment length has been completed, move to output folder
        try:
            result_path.replace(self.op.joinpath(result_path.name))
        except FileNotFoundError:
            pass


def run_fragment(tmp_dir="tmp",clear_tmp=True):
    """
    Directories must be in current execution directory
    """
    # initialize paths
    ip = Path.cwd()
    op = Path.cwd()
    tmp_path = Path.cwd().joinpath(tmp_dir)
    # check if output exists else create empty folder with output name
    op.mkdir(parents=True, exist_ok=True)
    tmp_path.mkdir(parents=True, exist_ok=True)
    # initialize the controller object
    ncbic = NCBIControler(ip, op)
    # for all folders in the temporary set
    # for every different fragment
    for filepath in ip.glob("BLAST_input*"):
        # Generate batches
        print(filepath)
        batch_path = ncbic.generate_fragment_batches(filepath)
        print(batch_path)
        # add boolean to run the ncbi blast
        ncbic.run_blast(batch_path, True)
    # If the clear is set
    if clear_tmp:
        # remove temporary files
        rmtree(tmp_path)

