
import os.path
import glob
from os import getcwd
from os.path import join, isdir, basename

def getListOfFiles(dirName): #Why can't we do it all in the same function?
    # create a list of file and sub directories
    # names in the given directory
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        elif fullPath.endswith("genomic.txt"):
            allFiles.append(fullPath)
    return allFiles
    
def icTyping_simulation(listOffiles):
    for file in listOffiles:
        print(str(file))             
        with open(file, 'r') as f:
            next(f) # advanced file pointer to next line
            dna_seq=f.read() #I read the file in order to work with it.
            dna_seq_new=dna_seq.replace('\n','') #line separators can break restriction sites,
            #so we need to replace newlines with anything in order to remove them.
            rs=dna_seq_new.replace('GAATTC','G/AATTC').replace('TTAA','T/TAA') #The program
            #will search the restiction sites and will mark the restricction place with /.
            fragm=rs.rsplit('/') #It splits the sequence in the restriction site.
            r1r2=[item for item in fragm if item.startswith('AATTCA') and item.endswith('GT')]
            r2r1=[item for item in fragm if item.startswith('TAAC') and item.endswith('TG')]                  
            fragm_seq=r1r2+r2r1
            bp1=[] #I create an empty list to fill it with n letters of each fragment (bp).
            for letter in r1r2:
                bp1.append(len(letter)+26) #I add the bp numbers to the empty list.        
            bp2=[]
            for letter in r2r1:
                bp2.append(len(letter)+28)    
            amplif=bp1+bp2
                #I select only the fragments that would be amplified by the primers. They act
                #both as Fw and Rv primers, so we need to take into account both options.  
            groups=sorted(amplif) #I order the fragments size from the smallest to the biggest.
            from collections import Counter #Establish the freq of every bp value.
            recounted=Counter(groups)            
        output = ""
        with open("fragm_seq_bp1_bp2.txt","a") as f:
            for sequence_bp1 in r1r2:
                file=basename(file)
                f.write("{},{},{},{}\n".format(file, sequence_bp1, len(sequence_bp1),len(sequence_bp1)+26))
            for sequence_bp2 in r2r1:
                file=basename(file)
                f.write("{},{},{},{}\n".format(file, sequence_bp2, len(sequence_bp2),len(sequence_bp2)+28))

#read the file
#return the sequence_bp2

                
