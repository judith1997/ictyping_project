import glob
import os
from Bio import Entrez
from Bio import SeqIO

listoffiles = glob.glob('*identifiers*')
output_file=open('genes_products_list.txt','a')

def gene_product(listoffiles):
    for file in listoffiles:
        print(str(file))
        output_file.write(str(file)+'\n')
        opened_file=open(file,'r')
        read_file=opened_file.readlines()
        read_file.pop(0)
        
        for group in read_file:
            separate_group=group.split()
            output_file.write(separate_group[0]+':'+'\t')
            Entrez.email = "judith1997@gmail.com"  # Always tell NCBI who you are
            handle = Entrez.efetch(db="nucleotide", id=str(separate_group[1]), rettype="gb", retmode="text",seq_start=int(separate_group[2]),seq_stop=int(separate_group[3]))
            text=handle.read()
            text_partition=text.split('\n')
            if 'pseudo' in text:
                product=[]
                gene=['/gene=pseudo']
                for element in text_partition:
                    element=element.strip()
                    if element.startswith('/product')==True:
                         product.append(element)
                output_file.write(str(gene)+str(product)+'\n')
                
            else:
                gene=[]
                product=[]
                seen_genes=[]
                for element in text_partition:
                    element=element.strip()
                    if element.startswith('/gene')==True and element not in seen_genes:
                        gene.append(element)
                        seen_genes.append(element)
                    if element.startswith('/product')==True:
                        product.append(element)
                output_file.write(str(gene)+str(product)+'\n')
    
    output_file.close()

