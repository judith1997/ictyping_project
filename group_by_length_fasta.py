def group_per_length(lecture):
    sep_values=[]                        #I create an empty list that i will fill with lists of 4 elements each
    for element in lecture:
        correct=element.replace('\n','')   
        fasta_format=correct.replace('GCA_','>') #FASTA format
        partition=fasta_format.rsplit(',')       # we obtain 4 elements for each line with the rsplit
        sep_values.append(partition)            #add as a list into the sep_values list

    sizes2=[]                                #empty list in which we will append the possible different sizes
    for element in sep_values:
        if element[3] not in sizes2:         #We don't consider repeated lengths. Element[3] is the length considering the adapter
            sizes2.append(element[3])

    general_list=[]                         #Create a list. Each element will be a list of all fragments of the same size2. Each fragment
    for element in sizes2:                  #is also a list of 4 values (name, seq, size1, size2), so it's a list of lists in which each
        group_list=[]                       #element is also a list.
        for part in sep_values:          
            if part[3]==element:
                group_list.append(part)
        general_list.append(group_list)

    for fragments_cluster in general_list:   #Write files for every set of fragments with a specific size
        filename='length_fragm_'+fragments_cluster[0][3]+'_'+str(len(fragments_cluster))+'.txt'
        createfile=open(filename,'w')
        for fragment in fragments_cluster:
            createfile.write(fragment[0])
            createfile.write('\n')
            createfile.write(fragment[1])
            createfile.write('\n')
        createfile.close()
