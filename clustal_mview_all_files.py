#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 10:12:28 2020

@author: judithsabatedelrio
"""
import glob
import os

listOffiles = glob.glob('length*.txt')

def clustal_msa(listOffiles):
    for file in listOffiles:

        count=len(open(file).readlines(  ))

        if count==2: #If there's only one sequence, don't align, cause it would give error.
            with open(file) as text_input:
                with open('MSA_'+file.split("_")[2]+'_'+file.split("_")[3]+'.FASTA', 'w') as text_output:
                    for line in text_input:
                        text_output.write(line)
                        #print(file)

        else:
            # path of the current script
            path = os.getcwd()

            # Before creating
            dir_list = os.listdir(path)
            #print("List of directories and files before creation:")
            #print(dir_list)
            #print()

            # Creates a new file
            with open('MSA_'+file.split("_")[2]+'_'+file.split("_")[3]+'.FASTA', 'w') as fp:
                pass
            # To write data to new file uncomment
            # this fp.write("New file created")

            # After creating
            dir_list = os.listdir(path)
            #print("List of directories and files after creation:")
            #print(dir_list)

            from Bio.Align.Applications import ClustalOmegaCommandline
            #define input file
            in_file = file
            out_file = 'MSA_'+file.split("_")[2]+'_'+file.split("_")[3]+'_'+'.FASTA'
            # define output file (I have tried just adding .phylip or no .format)
            # get the command for Clustal Omega
            # what I tried and what should work: outfmt = phylip
            clustalomega_cline = ClustalOmegaCommandline(infile = in_file, outfile = out_file, verbose = True, auto = False)

            # print the executable command
            os.system(str(clustalomega_cline)+" --force")
            buffer=os.popen(str("mview -in fasta {}".format('MSA_'+file.split("_")[2]+'_'+file.split("_")[3]+'_'+'.FASTA'))).read()
            # create_output=open(out_file,'w')
            # create_output.write(buffer)
            # create_output.close()

# The commandline will prompt you at this point to enter the line calling to your file, it should have this format:
# ./clustal-omega-1.2.3-macosx -i in_filename.fasta -o in_filename.phylip --auto -v
