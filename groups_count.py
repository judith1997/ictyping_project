#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  1 09:56:46 2020

@author: judithsabatedelrio
"""

import glob
import re
#Enter two different types of input files, ones 
#listOffiles_dist=glob.glob('distances*.txt') #names of fragm with distances between them
#listOffiles_seq=glob.glob('sequences*.txt') #names with their corresponding sequence

def id_groups(listOffiles_dist, listOffiles_seq):
    for file_dist in listOffiles_dist: #for every file with the names and distances
        with open(file_dist) as input_dist:#open the distances file
            lecture_dist=input_dist.readlines() #read every line separately as a string
            lecture_dist.pop(0) #Remove element 0, in this case, the title of the txt
            #Every line corresponds to two names and the distance between them.
            #lecture_dist=lecture_dist[:-1]
            distances=[] #Create an empty list where names and distances will be added
            for elementD in lecture_dist: #for every line (name1, name2, distance)
                partitionD=elementD.rsplit('\t') #create lists in which every item separated by a tab is an element
                distances.append(partitionD) #add the elements into the empty list. List of lists: name1, name2, dist.
    
        for file_seq in listOffiles_seq:#for every file with the name, sequence, and num fregm
            with open(file_seq) as input_seq: #open the file
                lecture_seq=input_seq.readlines() #read every line
                lecture_seq.pop(0) #remove the title
                #list in which each element is the name, seq and num of fragm.
                list_of_fragments=[] #create an empty list that will be a list of lists.
                list_of_names=[]
                for elementS in lecture_seq: #elementS is a string with name, seq, num
                    partitionS=elementS.rsplit('\t') #separate into 3 lists
                    list_of_fragments.append(partitionS) #List of lists:name, seq, num
                    list_of_names.append(partitionS[0])
        
                seen_fragments=[] #names seen
                groups=[] #each element is a list that contains fragm names
                repeticions=[] #Nombre de fragments per grup 
                if re.split('_',file_dist)[1] == re.split('_', file_seq)[1]:
        
                    for list_ in list_of_fragments: #each element is a list of the name, seq, num
                        contador=0
                        if list_[0] not in seen_fragments: #if the name is not seen yet, 
                            extractions=[]
                            extractions.append(list_[0]) #add it to the extraction list.
                            contador+=int(list_[2])
                            for item in distances: #For each name1, name2, distance
                                if list_[0]==item[0] and list_[0]!=item[1]: #if the name of the seq equals to one of the names of the distance file
                                    if float(item[2])<=0.2: #and if the distance is less than 0.2
                                        extractions.append(item[1]) #Add the second name to the extractions list
                                        seen_fragments.append(item[1]) #And add it also to the already seen names list
                                        indexa=list_of_names.index(item[1]) #funció index: de una llista, et busca el primer element que coincideixi amb el que tu poses entre parentesis i eto 
                                        contador+=int(list_of_fragments[indexa][2])
                            groups.append(extractions)#Add the extraction list to the empty list called groups
                        #Every sublist from groups list contains the names that have less than <0.2 distance between them as strings
                            seen_fragments.append(list_[0]) #Add the first name to the seen names
                            repeticions.append(contador)
        
                    with open ('groups_fragm'+file_dist.split('_')[1]+'_'+file_dist.split('_')[2],'w') as output1:
                        fulltext='' #Create an empty string
                        fulltext+='Num. fragments:'+str(len(list_of_fragments))+'\t'+'Num. groups:'+str(len(groups))+'\n' #Title: How many fragments are there and how many groups
                        counter=1 #Establish a counter to number each group
                        for i in range(len(groups)): #'i' is every group of names
                            fulltext+='/>Group'+str(counter)+'_'+str(repeticions[i])+':' #Name of the Group with fasta format an a  / symbol to split later
                            fulltext+=str(groups[i])+'\n'
                            counter=counter+1
                            
                            for element in list_of_fragments: #Every element is a list with a name and its corresponding seq
                                fulltext=fulltext.replace(element[0],element[1]) #it replaces the name for the sequence in the groups
                                fulltext=fulltext.replace('-','').replace('[', '').replace("'",'').replace(']','').replace(':','\n')
            #We remove punctuations to get a fasta format
    
                        output1.write(fulltext)
                        groups_list=fulltext.split('/') #We split the string into strings formed by all fragments in a group
                        groups_list.pop(0)
                        for group_seq in groups_list:
                            group_seq=group_seq.splitlines()
                            group_seq.pop(0)
                            group_seq=str(group_seq)
                            group_seq=group_seq.split(',')
                    with open ('pre_blast_'+file_dist.split('_')[1]+'_'+file_dist.split('_')[2],'w') as output2:
                        for gp in groups_list:
                            lines=gp.split(',') #We obtain a list of all the separated sequences of each group, the first with the group name
                            output2.write(str(lines[0]))
                            print(fulltext)
        
    
    
