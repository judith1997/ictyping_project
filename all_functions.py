#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 13 11:02:15 2020

@author: judithsabatedelrio
"""

import glob
from os import getcwd
from all_files_ictypingscript import icTyping_simulation, getListOfFiles
from group_by_length_fasta import group_per_length
from clustal_mview_all_files import clustal_msa
from tree_for_all_files import tree_for_all_files
from groups_count import id_groups
from rc_count import rc_pairwise
from ncbicontroller import run_fragment
from genbank_products import gene_product

if __name__ == '__main__':
    listOffiles=glob.glob('*genomic.txt')
    print(listOffiles)
    sequence_bp1=icTyping_simulation(listOffiles)
    print(sequence_bp1)
    oberture=open('fragm_seq_bp1_bp2.txt')
    lecture=oberture.readlines()
    grouped_fragments=group_per_length(lecture)
    listOffiles = glob.glob('length*.txt')
    print("list for clustal:", listOffiles)
    msa_clustal_omega=clustal_msa(listOffiles)
    listOffiles=glob.glob('*.FASTA')
    print("list for trees:", listOffiles)
    dist_seq=tree_for_all_files(listOffiles)
    listOffiles_dist=glob.glob('distancesMSA*.txt')
    listOffiles_seq = glob.glob("sequencesMSA*.txt")
    print("dist list for id_groups:", listOffiles_dist)
    print("seq list for id_groups:", listOffiles_seq)
    identity=id_groups(listOffiles_dist, listOffiles_seq)
    print(identity)
    listOffiles=glob.glob('pre_blast*.txt')
    print("list for RC:", listOffiles)
    rc_check=rc_pairwise(listOffiles)
    print(rc_check)
    run_blast=run_fragment()
    print(run_blast)
    listoffiles = glob.glob('*identifiers*')
    output_file=open('genes_products_list.txt','a')
    gene_product=gene_product(listoffiles)
    print(gene_product)
