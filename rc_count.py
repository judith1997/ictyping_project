#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  3 10:36:25 2020

@author: judithsabatedelrio
"""

#Check the RC of the representative sequences of each group. Pairwise sequence alignment.
'''
Calling the "score" method on the aligner with two sequences as arguments
will calculate the alignment score between the two sequences. Calling the "align" method on the aligner
with two sequences as arguments will return a generator yielding the alignments between the two sequences.
'''

from Bio import Align
from Bio.Seq import Seq
import glob
listOffiles=glob.glob('pre_blast*.txt') #input
#output_file=open('testRC_checked.txt','a') #output files
def rc_pairwise(listOffiles):
    for file in listOffiles: #for every input file
        print(str(file))
        opened_file=open(file,'r') #open the input file to read it
        read_file=opened_file.read()#convert the file content to a string
        file_part=read_file.split('>') #I separate the string into lists, each containing the group name and the seq.
        file_part.pop(0) #I remove the first line because it's an empty line.
        #file_part is a list of strings consisting of the group and the sequence.
        list_of_seq=[]
        list_of_rc_seq=[]
        list_of_seq1=[]
        list_of_rc_seq1=[]
        list_of_names=[]
        dict_of_counters={}
        print("filepart:",file_part)
        for element in file_part:
            #print(str(element))
            element_part=element.split('\n') #I separate every list into two sublists
            group_name=element_part[0].split('_')[0]
            dict_of_counters[group_name]=0
            seq = Seq(str(element_part[1])) #I write the sequence that i want to convert to rev compl
            sequence=str(element_part[1])#I write the original sequence
            rc_sequence=str(seq.reverse_complement())#It gets the rev compl seq.
            list_of_seq.append(sequence)
            list_of_rc_seq.append(rc_sequence)
            if sequence.startswith('AATT'): 
                sequence1 = sequence[4:]
            if sequence.endswith('AATT'):
                sequence1=sequence[:-4]
            if sequence.endswith('TA'):
                sequence1 = sequence[:-2]
            if sequence.startswith('TA'):
                sequence1=sequence[2:]       
            if rc_sequence.startswith('AATT'):
                rc_sequence1 = rc_sequence[4:]
            if rc_sequence.endswith('AATT'):
                rc_sequence1=rc_sequence[:-4]
            if rc_sequence.endswith('TA'):
                rc_sequence1 = rc_sequence[:-2]
            if rc_sequence.startswith('TA'):
                rc_sequence1=rc_sequence[2:] 
            list_of_seq1.append(sequence1)         
            list_of_rc_seq1.append(rc_sequence1)
            list_of_names.append(element_part[0])
            dict_of_counters[group_name]+=int(element_part[0].split('_')[1])
            
                
            #for name in list_of_names:
                #num=name.split('_')[1]
            #print(sequence)
                          
        print("rc_seqs:",list_of_rc_seq)
        print("seqs:",list_of_seq)
        seen_groups=[]
        # indices seems the easiers way to keep track of seen groups
        same_groups = [] # collection of groups
        for i in range(len(list_of_rc_seq1)):
            group_name=list_of_names[i].split('_')[0]
            for j in range(len(list_of_seq1)):
                # check if we dont compare the same element AND
                # check if we dont do a double check
                if i != j and not ((i,j) in seen_groups or (j,i) in seen_groups):
                    aligner = Align.PairwiseAligner() #Create a PairwiseAligner object that stores the match, mismatch and gap scores.
                    #By default, match score = 1; mismatch and gap = 0. Based on the values of the gap scores, the object chooses
                    #the appropriate alignment algorithm (Needleman-Wunsch, Smith-Waterman, Gotoh, or Waterman-Smith-Beyer global or local)
                    alignments = aligner.align(list_of_rc_seq1[i],list_of_seq1[j]) #Two input sequences as arguments.
                    score=alignments[0].score/len(sequence1) #The max score (100% identity) is the length of the seq, so we have to convert it to 1.0. The "score" method
                    #calculate the alignment score between the two seq.
                    score=round(score,2)
                    print("Score = %.1f:" % score)
                    print(alignments[0])
                    seen_groups.append((i,j))             
     
            #         output_file.write('rc'+rcseq[0]+'|'+seq[0]+':'+'\t')
            #         output_file.write(str(similitud(rcseq[1],seq[1]))+'\n')
            #         output_file.write('\n')
            # output_file.close()
                    # If the score is dissimilar enough add to list of unique groups
                    if score > 0.95:
                        same_groups.append((i,j))
                        dict_of_counters[group_name]+=int(list_of_names[j].split('_')[1])
                    
        with open ('BLAST_input'+file.split('_')[2]+'_'+file.split('_')[3],'a') as output1:
            count=1
            for group_number in range(len(list_of_seq)):
                # Remove one of the groups from the sequences:
                if not group_number in [same[1] for same in same_groups]:
                    fulltext='' #Create an empty string
                    # generate the elements to write
                    # +1 to adjust from python to "Sequence" Group names
                    group_name=str(list_of_names[group_number].split('_')[0])
                    calculate=dict_of_counters[group_name]/int(file.split('_')[3].split('.')[0])*100
                    perc=round(calculate,2)
                    fulltext+= ">Group"+str(count)+'_'+str(dict_of_counters[group_name])+'_'+str(perc)+'\n'+list_of_seq[group_number]+"\n"
                    count=count+1
                    output1.write(fulltext)
        print(seen_groups)
        print("# alignments done:",len(seen_groups))
        print(fulltext)

# listOffiles2=glob.glob('intermediate*.txt') #input
# def count_groups_again(listOffiles2):
#     for file2 in listOffiles2:
#         opened_file2=open(file2,'r')
#         lecture=opened_file2.read()
#         lec_list=opened_file2.readlines()
#         num_groups=len(lec_list)/2
#         with open ('BLAST_input'+file2.split('_')[1]+'_'+str(num_groups)+'groups'+'_'+file2.split('_')[2],'a') as output2:
#             output2.write(lecture)

     
    

#I0m deifining funtions to put all the commands in one script file.
#the score depends on the length of the fragment.
#it returns all the possible alignments, we should select the highest score.
#Now it is comparing each sequence with its revcompl, and it's not what we want.