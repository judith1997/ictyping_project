#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  1 09:56:46 2020

@author: judithsabatedelrio
"""

import glob
import re

def id_groups(listOffiles_dist, listOffiles_seq):
    for file_dist in listOffiles_dist:
        with open(file_dist) as input_dist:
            lecture_dist=input_dist.readlines() #read every line separately as a string
            lecture_dist.pop(0) #Remove element 0, in this case, the title of the txt
            #lecture_dist=lecture_dist[:-1]
            distances=[] #Create an empty list
            for element in lecture_dist:
                partition=element.rsplit('\t') #create lists in which every item separated by a tab is an element
                distances.append(partition) #add the elements into the empty list. List of lists: name1, name2, dist.

        for file_seq in listOffiles_seq:
            with open(file_seq) as input_seq:
                lecture_seq=input_seq.readlines()
                lecture_seq.pop(0)
                #lecture_seq=lecture_seq[:-1]
                list_of_fragments=[]
                for element in lecture_seq:
                    partition=element.rsplit('\t')
                    list_of_fragments.append(partition) #List of lists:name, seq

                paired_fragments=[]
                groups=[]

            if re.split('_',file_dist)[1] == re.split('_', file_seq)[1]:

                for element in list_of_fragments: #each element is the name and the seq
                    if element[0] not in paired_fragments: #if the name is not seen yet, add it to the extraction list
                        extractions=[]
                        extractions.append(element[0])
                        for item in distances: #For each name1, name2, distance
                            if element[0]==item[0] and element[0]!=item[1]: #if the name of the seq equals to one of the names of the distance file
                                if float(item[2])<=0.2: #and if the distance is less than 0.2
                                    extractions.append(item[1]) #Add the second name to the extractions list
                                    paired_fragments.append(item[1]) #And add it also to the already seen names list    
                                # else:
                                #     if len(groups)==0:                                    
                                       
                        groups.append(extractions)#Add the extraction list to the empty list called groups
                #Every sublist from groups list contains the names that have less than <0.2 distance between them as strings
                        paired_fragments.append(element[0]) #Add the first name to the seen names

                with open ('groups_fragm'+file_dist.split('_')[1]+'_'+'.txt','w') as output1:
                    fulltext='' #Create an empty string
                    fulltext+='Num. fragments:'+str(len(list_of_fragments))+'\t'+'Num. groups:'+str(len(groups))+'\n' #Title: How many fragments are there and how many groups
                    counter=1 #Establish a counter to number each group
                    for i in groups: #'i' is every group of names
                        fulltext+='/>Group'+str(counter)+':' #Name of the Group with fasta format an a  / symbol to split later
                        fulltext+=str(i)+'\n'
                        counter=counter+1

                        for element in list_of_fragments: #Every element is a list with a name and its corresponding seq
                            fulltext=fulltext.replace(element[0],element[1]) #it replaces the name for the sequence in the groups
                            fulltext=fulltext.replace('-','').replace('[', '').replace("'",'').replace(']','').replace(':','\n')
    #We remove punctuations to get a fasta format
                        groups_list=fulltext.split('/') #We split the string into strings formed by all fragments in a group
                        groups_list.pop(0)
                    output1.write(fulltext)

                with open ('pre_blast_'+file_dist.split('_')[1]+'_'+'.txt','w') as output2:
                    for gp in groups_list:
                        lines=gp.split(',') #We obtain a list of all the separated sequences of each group, the first with the group name
                        output2.write(str(lines[0]))
                        print(fulltext)



